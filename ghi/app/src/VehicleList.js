import { useState, useEffect } from "react";

export default function VehicleList({ models }) {
    function sortManus(a, b) {
        if (a.manufacturer.name.toUpperCase() < b.manufacturer.name.toUpperCase()) {
            return -1;
        }
        if (a.manufacturer.name.toUpperCase() > b.manufacturer.name.toUpperCase()) {
            return 1;
        }
        return 0;
    }

    return (
        <div className="my-5 container">
            <h1 className="text-left large-heading">Vehicle Models</h1>
            <table className="table table-striped align-middle">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Name</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.sort(sortManus).map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.name}</td>
                                <td className="w-25"><img src={model.picture_url} className="img-thumbnail img-fluid" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}