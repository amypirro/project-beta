export default function SalespeopleList({ salespeople }) {
    return (
        <div className="my-5 container">
            <h1 className="text-left large-heading">Salespeople</h1>
            <table className="table table-striped align-middle">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(sp => {
                        return (
                            <tr key={sp.id}>
                                <td>{sp.employee_id}</td>
                                <td>{sp.first_name}</td>
                                <td>{sp.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
