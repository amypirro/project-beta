import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleList from './VehicleList';
import VehicleModelForm from './VehicleModelForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import SalesHistory from './SalesHistory';
import ServiceAppointmentForm from './ServiceForm';
import AppointmentList from './ServicAppointments';
import ServiceHistory from './ServiceHistory';

function App() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);

  const getAutomobiles = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/')
    if (response.ok) {
      const data = await response.json()
      setAutomobiles(data.autos)
    }
  }

  async function getSalespeople() {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getCustomers() {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  const getManufacturers = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/')

    if (response.ok) {
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }

  async function getModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  useEffect(() => {
    getAutomobiles();
    getSalespeople();
    getCustomers();
    getSales();
    getManufacturers();
    getModels();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="vehicles" >
          <Route index element={<VehicleList models={models} />} />
          <Route path="new" element={<VehicleModelForm manufacturers={manufacturers} getModels={getModels} />} />
        </Route>
        <Route path="manufacturer">
          <Route index element={<ManufacturerList manufacturers={manufacturers} />} />
          <Route path='new' element={<ManufacturerForm getManufacturers={getManufacturers} />} />
        </Route>
        <Route path="inventory">
          <Route index element={<AutomobileList automobiles={automobiles} />} />
          <Route path='new' element={<AutomobileForm models={models} getAutomobiles={getAutomobiles} />} />
        </Route>
        <Route path="salespeople">
          <Route index element={<SalespeopleList salespeople={salespeople} />} />
          <Route path='new' element={<SalespersonForm getSalespeople={getSalespeople} />} />
        </Route>
        <Route path="customers">
          <Route index element={<CustomerList customers={customers} />} />
          <Route path='new' element={<CustomerForm getCustomers={getCustomers} />} />
        </Route>
        <Route path="sales">
          <Route index element={<SalesList sales={sales} />} />
          <Route path='new' element={<SalesForm getSales={getSales} getAutomobiles={getAutomobiles} automobiles={automobiles} customers={customers} salespeople={salespeople} />} />
          <Route path='history' element={<SalesHistory sales={sales} salespeople={salespeople} />} />
        </Route>
        <Route path="technician">
          <Route index element={<TechnicianList />} />
          <Route path='new' element={<TechnicianForm />} />
        </Route>
        <Route path="appointment">
          <Route index element={<AppointmentList />} />
          <Route path='new' element={<ServiceAppointmentForm />} />
          <Route path='history' element={<ServiceHistory />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
