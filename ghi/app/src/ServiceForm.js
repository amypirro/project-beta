import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom"

function ServiceAppointmentForm() {

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.vin = vin
        data.customer = customer
        data.date_time = new Date(date).toISOString()
        data.technician = technician
        data.reason = reason

        const appointmentUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            const newTechnician = await response.json()

            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setTechnician('')
            setReason('')
            setHasSubmitted(true)
        }
    }

    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technician)
        }
    }
    const [hasSubmitted, setHasSubmitted] = useState(false)
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value)
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }
    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Create a service appointment</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" value={vin} id="vin" className="form-control" />
                                <label htmlFor="vin">Vin Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleCustomerChange} placeholder="customer" required type="text" name="Customer" value={customer} id="customer" className="form-control" />
                                <label htmlFor="customer">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleDateChange} placeholder="date" required type="datetime-local" name="date" value={date} id="date" className="form-control" />
                                <label htmlFor="date_time">Date of appointment</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleTechnicianChange} required id="technician" name="technician" value={technician} className="form-select">
                                    <option value="">Choose Technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.employee_id} value={technician.employee_id}>
                                                {technician.first_name + " " + technician.last_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleReasonChange} placeholder="reason" required type="text" name="reason" value={reason} id="reason" className="form-control" />
                                <label htmlFor="reason">Reason for Appointment</label>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                        <div className={messageClasses} id='success-message'>
                            <p>You have successfully added a service appointment.</p>
                            <Link to="/appointment/"><button className="btn btn-light btn-sm">See all service appointments</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another appointment</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ServiceAppointmentForm