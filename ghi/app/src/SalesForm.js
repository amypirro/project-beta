import { useState } from "react";
import { Link } from "react-router-dom";

export default function SalesForm({ getSales, getAutomobiles, automobiles, salespeople, customers }) {
    const [hasSubmitted, setHasSubmitted] = useState(false);

    const [formData, setFormData] = useState({
        price: '',
        automobile: '',
        salesperson: '',
        customer: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const vin = formData.automobile;
            const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const autoFetchConfig = {
                method: "put",
                body: JSON.stringify({ "sold": true }),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const autoResponse = await fetch(autoUrl, autoFetchConfig);

            setFormData({
                price: '',
                automobile: '',
                salesperson: '',
                customer: '',
            });
            setHasSubmitted(true);
            getSales();
            getAutomobiles();

        }
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    let noneForSale = automobiles.filter(auto => auto.sold === false).length < 1;

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-sale-form">
                            <div className="mb-3 mt-3">
                                <label htmlFor="automobile">Automobile VIN</label>
                                {noneForSale && <p className="mt-2 alert alert-danger" role="alert" style={{ fontSize: "12px", padding: "3px 10px" }}>There are no automobiles for sale.</p>}
                                <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Choose an automobile VIN</option>
                                    {automobiles.filter(auto => auto.sold === false).map(auto => {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>
                                                {auto.vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3 mt-3">
                                <label htmlFor="salesperson">Salesperson</label>
                                <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                                    <option value="">Choose a salesperson</option>
                                    {salespeople.map(sp => {
                                        return (
                                            <option key={sp.id} value={sp.id}>
                                                {sp.first_name} {sp.last_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3 mt-3">
                                <label htmlFor="customer">Customer</label>
                                <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                                    <option value="">Choose a customer</option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.first_name} {customer.last_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <label htmlFor="price">Price</label>
                            <div className="mb-3 input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">$</span>
                                </div>
                                <input onChange={handleFormChange} value={formData.price} required type="number" name="price" id="price" className="form-control" />
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id="success-message">
                            <p>Congrats on the sale!</p>
                            <Link to="/sales/"><button className="btn btn-light btn-sm">See all sales</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another sale</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}