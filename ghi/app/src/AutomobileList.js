import { useState, useEffect } from "react";

function AutomobileList({ automobiles }) {
    const [soldStatus, setSoldStatus] = useState(false)

    const handleClick = () => {
        setSoldStatus(!soldStatus)
    }

    let autos = automobiles;
    if (soldStatus) {
        autos = automobiles.filter(auto => auto.sold === false)
    }

    const buttonText = soldStatus ? "Show all" : "Show available for sale"
    const headingText = soldStatus ? "Automobiles for sale" : "All Automobiles"

    return (
        <div className="container my-5">
            <div className="d-flex justify-content-between align-items-center">
                <h1 className="text-left large-heading">
                    {headingText}
                </h1>
                <button onClick={handleClick} className="btn btn-sm btn-primary mb-2" type="button">{buttonText}</button>
            </div>
            <table className="table table-stripped table-hover align-middle">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(automobile => {
                        return (
                            <tr key={automobile.id}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name} </td>
                                <td>{automobile.sold ? "Yes" : "No"}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default AutomobileList