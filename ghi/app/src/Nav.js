import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className='dropdown-item' to="/manufacturer/">Manufacturers</NavLink>
                <NavLink className="dropdown-item" to="/manufacturer/new/">Add a Manufacturer</NavLink>
                <hr className="dropdown-divider" />
                <NavLink className='dropdown-item' to="/vehicles/">Models</NavLink>
                <NavLink className="dropdown-item" to="/vehicles/new/">Add a Model</NavLink>
                <hr className="dropdown-divider" />
                <NavLink className='dropdown-item' to="/inventory/">Automobiles</NavLink>
                <NavLink className="dropdown-item" to="/inventory/new/">Add an Automobile</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className='dropdown-item' to="/sales/">Sales</NavLink>
                <NavLink className="dropdown-item" to="/sales/new/">Add a Sale</NavLink>
                <hr className="dropdown-divider" />
                <NavLink className='dropdown-item' to="/customers/">Customers</NavLink>
                <NavLink className="dropdown-item" to="/customers/new/">Add a Customer</NavLink>
                <hr className="dropdown-divider" />
                <NavLink className='dropdown-item' to="/salespeople/">Salespeople</NavLink>
                <NavLink className="dropdown-item" to="/salespeople/new/">Add a Salesperson</NavLink>
                <NavLink className="dropdown-item" to="/sales/history/">Salesperson History</NavLink>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className='dropdown-item' to="/appointment/">Service Appointments</NavLink>
                <NavLink className="dropdown-item" to="/appointment/new/">Add a Service Appointment</NavLink>
                <NavLink className='dropdown-item' to="/appointment/history/">Service History</NavLink>
                <hr className="dropdown-divider" />
                <NavLink className='dropdown-item' to="/technician/">Technicians</NavLink>
                <NavLink className="dropdown-item" to="/technician/new/">Add a Technician</NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
