import { useState } from "react";
import { Link } from "react-router-dom";

export default function CustomerForm({ getCustomers }) {
    const [hasSubmitted, setHasSubmitted] = useState(false);

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/customers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                phone_number: '',
                address: '',
            });
            setHasSubmitted(true);
            getCustomers();

        }
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }
    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Add a Customer</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-salesperson-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id="success-message">
                            <p>You have successfully added a customer.</p>
                            <Link to="/customers/"><button className="btn btn-light btn-sm">See all customers</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another customer</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}