import MainPageCard from "./MainPageCard";
import { FluentEmojiHighContrastMechanic, GameIconsMechanicGarage, IconParkSolidSalesReport, RaphaelCustomer, ClarityEmployeeGroupSolid, CarbonCar, BiBuildingFillGear, ClarityAutoSolid, WhhAppointment } from "./MainPageCard";

function MainPage() {
  return (
    <>
      <div className="home-hero px-4 py-5 text-center">
        <h1 className="cover-heading display-5 fw-bold">CarCar</h1>
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
      </div>
      <div className="container mt-5">
        <div className="row">
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<BiBuildingFillGear width="5em" height="100" />} path="/manufacturer/" category="Manufacturers" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<CarbonCar width="5em" height="100" />} path="/vehicles/" category="Vehicle Models" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<ClarityAutoSolid width="5em" height="100" />} path="/inventory/" category="Automobiles" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<ClarityEmployeeGroupSolid width="5em" height="100" />} path="/salespeople/" category="Salespeople" />
          </div>
        </div>
        <div className="row mb-5 mt-lg-4">
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<RaphaelCustomer width="5em" height="100" />} path="/customers/" category="Customers" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<IconParkSolidSalesReport width="5em" height="100" />} path="/sales/" category="Sales" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<FluentEmojiHighContrastMechanic width="5em" height="100" />} path="/technician/" category="Technicians" />
          </div>
          <div className="col-12 col-lg-3 col-sm-6 mb-lg-0 mb-4">
            <MainPageCard icon={<GameIconsMechanicGarage width="5em" height="100" />} path="/appointment/" category="Service Appts" />
          </div>
        </div>
      </div>
    </>
  );
}

export default MainPage;