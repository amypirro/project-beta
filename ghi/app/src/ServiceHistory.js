import { useState, useEffect } from "react";

function ServiceHistory() {

    const [appointments, setAppointments] = useState([])
    const [filterData, setFilterData] = useState('')

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointment)
        }
    }

    const handleFilter = (event) => {
        const searchWord = event.target.value
        setFilterData(searchWord)
        }
        let filtered = appointments
        if (filterData.length > 0) {
            filtered = appointments.filter(appt => appt.vin.toUpperCase().includes(filterData.toUpperCase()))
        }

    useEffect(() => {
        fetchData()
    }, [])




    return(
        <div className="my-5 container">
            <h1 className="text-left large-heading">Service History</h1>
            <input type="text" onChange={handleFilter} placeholder="Search a VIN" value={filterData} data={appointments} />
            <table className="table table-stripped table-hover align-middle">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filtered.map(appointment => {
                        return(
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip ? "Yes" : "No"}</td>
                                <td>{appointment.customer}</td>
                                <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                                <td>{ new Date(appointment.date_time).toLocaleTimeString([], {
                                    hour: "2-digit",
                                    minute: "2-digit",
                                })}</td>
                                <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>{appointment.status}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ServiceHistory