import React, { useState } from "react";
import { Link } from "react-router-dom"

function TechnicianForm() {

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = first
        data.last_name = last
        data.employee_id = id

        const technicianUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'content-type': 'application/json',
            }
        }

        const response = await fetch(technicianUrl, fetchConfig)
        if (response.ok) {
            const newTechnician = await response.json()

            setFirst('')
            setLast('')
            setId('')
            setHasSubmitted(true)
        }
    }

    const [hasSubmitted, setHasSubmitted] = useState(false)
    const [first, setFirst] = useState('')
    const [last, setLast] = useState('')
    const [id, setId] = useState('')

    const handleFirstChange = (event) => {
        const value = event.target.value
        setFirst(value)
    }
    const handleLastChange = (event) => {
        const value = event.target.value
        setLast(value)
    }
    const handleIdChange = (event) => {
        const value = event.target.value
        setId(value)
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return (
        <div className="container my-5">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Add a Technician</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstChange} placeholder="first_name" required type="text" name="first_name" value={first} id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastChange} placeholder="last_name" required type="text" name="last_name" value={last} id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleIdChange} placeholder="employee_id" required type="number" name="employee_id" value={id} id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id="success-message">
                            <p>You have successfully added a technician.</p>
                            <Link to="/technician/"><button className="btn btn-light btn-sm">See all technicians</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another technician</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TechnicianForm