import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function AutomobileForm({ getAutomobiles, models }) {
    const [hasSubmitted, setHasSubmitted] = useState(false);

    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        let newValue = value

        if (inputName === "vin") {
            newValue = e.target.value.toUpperCase();
        }

        setFormData({
            ...formData,
            [inputName]: newValue
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
            setHasSubmitted(true);
            getAutomobiles();
        }
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    function sortManus(a, b) {
        if (a.manufacturer.name.toUpperCase() < b.manufacturer.name.toUpperCase()) {
            return -1;
        }
        if (a.manufacturer.name.toUpperCase() > b.manufacturer.name.toUpperCase()) {
            return 1;
        }
        return 0;
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Add an automobile to inventory</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-vehicle-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.year} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.vin} placeholder="VIN" required type="text" maxLength="17" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model" className="form-select">
                                    <option value="">Choose a model</option>
                                    {models.sort(sortManus).map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.manufacturer.name} {model.name}
                                            </option>
                                        );
                                    })}
                                </select>
                                <p className="mt-2" style={{ fontSize: "12px", color: "grey" }}>Don't see the model you need? <Link to="/vehicles/new/">Create model</Link></p>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id="success-message">
                            <p>You have successfully added an automobile.</p>
                            <Link to="/inventory/"><button className="btn btn-light btn-sm">See all vehicles in inventory</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another vehicle</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}