import { useState } from "react";

export default function SalesHistory({ sales, salespeople }) {
    const [selection, setSelection] = useState("");

    const handleSelection = (e) => {
        const value = e.target.value;
        setSelection(value);
    }

    const selected = selection && selection.length > 0;
    const hasSales = sales.filter(sale => sale.salesperson.employee_id === selection).length > 0;

    let noSaleClass = 'd-none';
    if (!hasSales && selected) {
        noSaleClass = '';
    }

    return (
        <div className="my-5 container">
            <h1>Salesperson History</h1>
            <select onChange={handleSelection} value={selection} name="selection" className="form-select mt-3">
                <option value="">Choose a salesperson</option>
                {salespeople.map(sp => {
                    return (
                        <option key={sp.employee_id} value={sp.employee_id}>
                            {sp.first_name} {sp.last_name}
                        </option>
                    );
                })}
            </select>

            <table className="table table-striped align-middle mt-2">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter(sale => sale.salesperson.employee_id === selection).map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={noSaleClass}>
                <p className="text-center">There are no sales for this salesperson.</p>
            </div>
            {!selected && <p className="text-center">Please select a salesperson to see their sales history.</p>}
        </div>
    )
}