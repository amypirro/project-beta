import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function VehicleModelForm({ getModels, manufacturers }) {
    const [hasSubmitted, setHasSubmitted] = useState(false);

    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
            setHasSubmitted(true);
            getModels();

        }
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Create a vehicle model</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-vehicle-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            {formData.picture_url.length > 200 && <p className="mt-2 alert alert-danger" role="alert" style={{ fontSize: "12px", padding: "3px 10px" }}>URL must be 200 characters or less.</p>}
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer" className="form-select">
                                    <option value="">Choose a manufacturer</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                                <p className="mt-2" style={{ fontSize: "12px", color: "grey" }}>Don't see the manufacturer you need? <Link to="/manufacturer/new/">Create manufacturer</Link></p>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id="success-message">
                            <p>You have successfully added a vehicle model.</p>
                            <Link to="/vehicles/"><button className="btn btn-light btn-sm">See all vehicle models</button></Link>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another model</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}