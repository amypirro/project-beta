import React, { useState } from "react"
import { Link } from "react-router-dom"

function ManufacturerForm({ getManufacturers }) {
    const [manufacturer, setManufacturer] = useState('')
    const [hasSubmitted, setHasSubmitted] = useState(false)

    const handleNameChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = manufacturer

        const manufacturerURL = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'content-type': 'application/json',
            }
        }

        const response = await fetch(manufacturerURL, fetchConfig)
        if (response.ok) {
            const newManufacturer = await response.json()

            setManufacturer('')
            getManufacturers();
            setHasSubmitted(true)
        }
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSubmitted) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return (
        <div className="container my-5">
            <div className="row">
                <div className="offset-2 col-8">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-left large-heading">Create a new Manufacturer</h1>
                        <form onSubmit={handleSubmit} className={formClasses} id="create-manufacturer-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} placeholder="Manufacturer" required type="text" name="manufacturer" value={manufacturer} id="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer Name...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className={messageClasses} id='success-message'>
                            <p>You have successfully added a manufacturer.</p>
                            <div><Link to="/manufacturer/"><button className="btn btn-light btn-sm">See all manufacturers</button></Link></div>
                            <div className="mt-3"><button onClick={() => {setHasSubmitted(false)}} className="btn btn-success btn-sm">Add another manufacturer</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm