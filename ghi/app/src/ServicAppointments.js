import React, {useState, useEffect } from "react"


function AppointmentList() {
    const[appointments, setAppointments] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json()
                setAppointments(data.appointment)
            }
        }

    useEffect(() => {
        fetchData()
    }, [])

    const cancelAppointment = async (id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(cancelUrl,fetchConfig)
        if (response.ok) {
            fetchData()
        }
    }

    const finishAppointment = async (id) => {
        const finishUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(finishUrl, fetchConfig)
        if (response.ok) {
            fetchData()
        }
    }


    return (
        <div className="my-5 container">
            <h1 className="text-left large-heading">Service Appointments</h1>
            <table className="table table-stripped table-hover align-middle">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        if(appointment.status === "Created")
                        return (
                            <tr key = {appointment.id}>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.vip ? "Yes" : "No" }</td>
                                <td>{ appointment.customer }</td>
                                <td>{new Date(appointment.date_time).toLocaleDateString() }</td>
                                <td>{new Date(appointment.date_time).toLocaleTimeString([], {
                                    hour: "2-digit",
                                    minute: "2-digit",
                                })}</td>
                                <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>
                                    <button type="button" className="btn btn-danger button-hover" id={appointment.id} onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-success button-hover" id={appointment.id} onClick={() => finishAppointment(appointment.id)}>Finish</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default AppointmentList