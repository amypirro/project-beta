import { useState, useEffect } from "react";

function ManufacturerList({ manufacturers }) {
    function sortManus(a, b) {
        if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return -1;
        }
        if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return 1;
        }
        return 0;
    }

    return (
        <div className="my-5 container">
            <h1 className="text-left large-heading">
                Manufacturers
            </h1>

            <table className="table table-stripped table-hover align-middle">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.sort(sortManus).map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ManufacturerList
