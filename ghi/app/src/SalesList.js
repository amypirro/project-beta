import { useState } from "react";

export default function SalesList({ sales }) {
    const [search, setSearch] = useState("");

    const handleSearch = (e) => {
        const value = e.target.value;
        setSearch(value);
    }
    let filtered = sales
    if (search.length > 0) {
        filtered = sales.filter((sale) => {
            return sale.automobile.vin.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.salesperson.first_name.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.salesperson.last_name.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.salesperson.employee_id.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.customer.first_name.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.customer.last_name.toLowerCase().startsWith(search.toLowerCase()) ||
                sale.price.startsWith(search)
        });
    }

    return (
        <div className="my-5 container">
            <div className="row align-items-center">
                <div className="col-12 col-md-6">
                    <h1 className="text-left large-heading">Sales</h1>
                </div>
                <div className="col-12 col-md-6">
                    <input placeholder="Search" onChange={handleSearch} value={search} required type="text" name="search" className="form-control mb-1" />
                </div>
            </div>
            <table className="table table-striped align-middle mt-2">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filtered.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}