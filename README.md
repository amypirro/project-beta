# CarCar

![CarCar home page](image/CarCar_homepage.png)

Team:

- [Everest Bradford](https://www.linkedin.com/in/everest-bradford/ "Everest Bradford LinkedIn") - Services
- [Amy Pirro](https://www.linkedin.com/in/amypirro/ "Amy Pirro LinkedIn") - Sales

## How to Run this App / Getting Started

Requirements: Docker Desktop, Git

1. Fork this repository

2. Clone repository
   `git clone <repo_url>`

3. Run these commands in the relevant directory (where you cloned the repo):

```
docker volume create beta-data
docker-compose build
docker-compose up
```

Make sure all Docker containers are running.

5. In Docker Desktop, in the project-beta-sales-api-1 container, on Exec tab, run this command:
   `python manage.py migrate`

6. In Docker Desktop, in the project-beta-service-api-1 container, on Exec tab, run this command:
   `python manage.py migrate`

7. View the app in your browser at http://localhost:3000/

## Diagram

![CarCar diagram](image/CarCar_application_diagram-Amy-Everest.png)

## Service microservice

The service microservice three models:

The AutomobileVO Model: The AutomobileVO object is a value object that represents all of the wonderful vehicles we have for sale in our inventory model. It has fields for the VIN number of each vehicle, their sold status, and an href or link to the given vehicle.

The Technician Model: The Technician Model represents the heart and soul of our staff, our technicians. There are fields for their first names, last names, and their employee ID's

The Appointment Model: The Appointment Model represents any service appointment that is created with us here at CarCar. It has fields for the date and time of your appointment, the reason for your visit, the status of your visit (created, canceled, or finished), the VIN number of your vehicle, the customer name (You), the wonderful technician working on your vehicle, and lastly if your car was bought from us, resulting in a VIP status.

Explain your models and integration with the inventory
microservice, here.

### Service API

All endpoints can be accessed through Insomnia.

### TECHNICIANS - The incredible back of house team that keeps your vehicle functioning and on the road.

| Action              | Method | URL                                             |
| ------------------- | ------ | ----------------------------------------------- |
| List technicians    | GET    | http://localhost:8080/api/technicians/          |
| Create a technician | POST   | http://localhost:8080/api/technicians/          |
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/ |

LIST TECHNICIANS: This is a GET request that will give you a list of all of our lovely technicians that are currently employed. It will show their first names, last names, and employee ID's for each employee. No data input is needed for this view to function. If your containers are running, then just use the link above in insomnia to see all of our lovely technicians.

```
EXAMPLE:
{
	"first_name": "Jonathan",
	"last_name": "Toews",
	"employee_id": 1919
}
```

CREATE A TECHNICIAN: This is a POST request that handles creating a new technician. To use this endpoint, copy the link from above, and enter the data in the format depicted below. The employee_id field will only allow numbers for the ID.

```
{
	"first_name": "Patrick",
	"last_name": "Kane",
	"employee_id": "8888"
}
```

As you can see, the format to enter data is the same way it will be shown when it is listed out in the list view above. We assigned the "first_name" field to Patrick, we assigned the 'last_name" field to Kane, and made his employee id 8888. If I were to go use the list view above to now show all of my incredible technicians, it would look something like this:

```
EXAMPLE:
{
	"first_name": "Jonathan",
	"last_name": "Toews",
	"employee_id": 1919
},
{
	"first_name": "Patrick",
	"last_name": "Kane",
	"employee_id": 8888
}
```

DELETE A TECHNICIAN: This is a DELETE request that handles removing a technician from our system. Hopefully this feature wont need to ever be used, however if you do need it, use the endpoint posted above. In the ID section of the endpoint, input the employees id number. Once they are removed from the system, a message will appear like the one posted below:

```
{
	"deleted": true
}
```

Thats it!! Now your able to view all your technicians, create a new technician, and if need be, delete a technician from the system. Remember, in any end point that asks for an ID, be sure to use the employees ID number to make sure your working with the correct technician.

### SERVICE APPOINTMENTS - What keeps your vehicle functioning and on the road

| Action                                | Method | URL                                              |
| ------------------------------------- | ------ | ------------------------------------------------ |
| List service appointments             | GET    | http://localhost:8080/api/appointments/          |
| Create service appointment            | POST   | http://localhost:8080/api/appointments/          |
| Delete service appointment            | DELETE | http://localhost:8080/api/appointment/<int:id>   |
| Change appointment status to canceled | PUT    | http://localhost:8080/api/appointments/1/cancel/ |
| Change appointment status to finished | PUT    | http://localhost:8080/api/appointments/1/finish/ |

LIST SERVICE APPOINTMENTS: This is a GET request that handles showing a list of all of the service appointments that have ever been booked with us here at CarCar. It will show the ID of the appointment, the date and time of the appointment, reason for the appointment, status of the appointment, the VIP status (if the car was bought from us), the VIN of the vehicle, the customers name, and the incredible technician who will be working on the vehicle. When send the GET request to the endpoint listed above, your data response should like something like this:

```
EXAMPLE:
	"appointment": [
		{
			"id": 9,
			"date_time": "2023-12-14T18:11:00+00:00",
			"reason": "Because I want to",
			"status": "Created",
			"vip": false,
			"vin": "2C3CDZBT3PH504050",
			"customer": "Jonathan Toews",
			"technician": {
				"first_name": "Patrick",
				"last_name": "Kane",
				"employee_id": 8888
			}
		},
```

CREATE SERVICE APPOINTMENT: This is a POST request that handles adding a new vehicle to the database. To use this endpoint, make sure your request method is POST, and enter the data in the format depicted below:

```
EXAMPLE:
{
	"date": "2023-04-20",
	"time": "14:39:00.000",
	"reason": "Brakes",
	"vin": "JTLZE4FE8CJ023435",
	"customer": "Jonathan Toews",
	"technician": "1234"
}
```

Please note that the dates and times must be entered in the format shown above. If they are entered any other way, you will receive an error. Also, upon creation of an appointment, the program will automatically assign an ID number to the appointment. This ID number is useful if you need to delete or change an appointment.

DELETE SERVICE APPOINTMENT: This is a DELETE request that handles removing an appointment from the system. To be able to use this endpoint, you'll have to use the ID for the given appointment (found in the data response from the List Service Appointment endpoint) in the endpoint url. Once the request is sent, you should see a message like the following below:

```
EXAMPLE:
{
	"deleted": true
}
```

CHANGE APPOINTMENT STATUS TO CANCELED: This is a PUT request that changes the status of a current appointment to "Canceled". To use this endpoint, make sure your request is a "PUT" request and that the ID in the endpoint url matches the ID of your appointment (found in the data response from the List Service Appointment endpoint). Below is an example of how the status change works.

```
EXAMPLE - Before request is sent:
{
			"id": 9,
			"date_time": "2023-12-14T18:11:00+00:00",
			"reason": "Because I want to",
			"status": "Created",
			"vip": false,
			"vin": "2C3CDZBT3PH504050",
			"customer": "Jonathan Toews",
			"technician": {
				"first_name": "Patrick",
				"last_name": "Kane",
				"employee_id": 8888
			}
		},
EXAMPLE - After request is sent:
{
			"id": 9,
			"date_time": "2023-12-14T18:11:00+00:00",
			"reason": "Because I want to",
			"status": "Canceled",
			"vip": false,
			"vin": "2C3CDZBT3PH504050",
			"customer": "Jonathan Toews",
			"technician": {
				"first_name": "Patrick",
				"last_name": "Kane",
				"employee_id": 8888
			}
		},
```

Note the only thing to change was the status of the appointment. This endpoint will not allow you to update anything outside of the status

CHANGE APPOINTMENT STATUS TO FINISHED: Just like the canceled version above, this is a PUT request that changes the status of a current appointment to "Finished". To use this endpoint, make sure your request is a "PUT" request and that the ID in the endpoint url matches the ID of your appointment (found in the data response from the List Service Appointment endpoint). Below is an example of how the status change works.

```
EXAMPLE - Before request is sent:
{
			"id": 9,
			"date_time": "2023-12-14T18:11:00+00:00",
			"reason": "Because I want to",
			"status": "Created",
			"vip": false,
			"vin": "2C3CDZBT3PH504050",
			"customer": "Jonathan Toews",
			"technician": {
				"first_name": "Patrick",
				"last_name": "Kane",
				"employee_id": 8888
			}
		},
EXAMPLE - After request is sent:
{
			"id": 9,
			"date_time": "2023-12-14T18:11:00+00:00",
			"reason": "Because I want to",
			"status": "Finished",
			"vip": false,
			"vin": "2C3CDZBT3PH504050",
			"customer": "Jonathan Toews",
			"technician": {
				"first_name": "Patrick",
				"last_name": "Kane",
				"employee_id": 8888
			}
		},
```

### Service Value Objects

The only Value Object in this microservice is the AutomobileVO. As stated above, it represents all the amazing automobiles on our lot. It holds the VIN, sold status, and a link to each individual automobile.

## Sales microservice

The sales microservice has four models:

- AutomobileVO: a value object, AutomobileVO objects are created or updated based on Automobile objects in the inventory microservice using a poller. They have `vin` and `sold` properties.

- Salesperson: represents a salesperson employee and has `first_name`, `last_name`, and `employee_id` properties

- Customer: represents a customer and has `first_name`, `last_name`, `address`, and `phone_number` properties

- Sale: represents the sale of an automobile and relies on the other three models to be created (`automobile`, `salesperson`, and `customer` properties), and also has a `price` property.

### Sales Microservice API

All endpoints can be accessed through Insomnia.

### Salespeople

| Action             | Method | URL                                        |
| ------------------ | ------ | ------------------------------------------ |
| List salespeople   | GET    | http://localhost:8090/api/salespeople/     |
| Create salesperson | POST   | http://localhost:8090/api/salespeople/     |
| Delete salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |

To create a Salesperson (send this JSON body):

```
{
    "first_name": "Ann",
    "last_name": "Jones",
    "employee_id": "ajones"
}
```

Return value of creating a Salesperson:

```
{
    "first_name": "Ann",
    "last_name": "Jones",
    "employee_id": "ajones",
    "id": 1
}
```

Return value of List Salespeople

```
{
    "salespeople": [
        {
            "first_name": "Ann",
            "last_name": "Jones",
            "employee_id": "ajones",
            "id": 1
        },
        {
            "first_name": "John",
            "last_name": "Smith",
            "employee_id": "jsmith",
            "id": 2
        },
    ]
}
```

Deleting a Salesperson:
Send a DELETE request to http://localhost:8090/api/salespeople/:id/, where :id is replaced with the id value of the Salesperson to be deleted. No JSON body required. On first request, a message will be returned confirming the deletion.

### Customers

| Action          | Method | URL                                      |
| --------------- | ------ | ---------------------------------------- |
| List customers  | GET    | http://localhost:8090/api/customers/     |
| Create customer | POST   | http://localhost:8090/api/customers/     |
| Delete customer | DELETE | http://localhost:8090/api/customers/:id/ |

To create a Customer (send this JSON body):

```
{
    "first_name": "Jane",
    "last_name": "Smith",
    "address": "123 Main Street, Town, CA 12345",
    "phone_number": "123-123-1234"
}
```

Return value of creating a Customer:

```
{
    "first_name": "Jane",
    "last_name": "Smith",
    "address": "123 Main Street, Town, CA 12345",
    "phone_number": "123-123-1234",
    "id": 1
}
```

Return value of List Customers:

```
{
    "customers": [
        {
            "first_name": "Jane",
            "last_name": "Smith",
            "address": "123 Main Street, Town, CA 12345",
            "phone_number": "123-123-1234"
            "id": 1
        },
        {
            "first_name": "Sansa",
            "last_name": "Stark",
            "address": "12 Direwolf Lane, Winterfell, ME 11111",
            "phone_number": "555-123-4567",
            "id": 2
        }
    ]
}
```

Deleting a Customer:
Send a DELETE request to http://localhost:8090/api/customers/:id/, where :id is replaced with the id value of the Customer to be deleted. No JSON body required. On first request, a message will be returned confirming the deletion.

### Sales

| Action      | Method | URL                                 |
| ----------- | ------ | ----------------------------------- |
| List sales  | GET    | http://localhost:8090/api/sales/    |
| Create sale | POST   | http://localhost:8090/api/sales/    |
| Delete sale | DELETE | http://localhost:8090/api/sales/:id |

To create a Sale (send this JSON body):

```
{
    "price": 13.99,
    "automobile": "THISISMYPINKCAR99",
    "salesperson": 4,
    "customer": 3
}
```

- automobile must have a value of an existing Automobile vin string
- salesperson must have a value of an existing Salesperson id
- customer must have a value of an existing Customer id

Return value of creating a Sale:

```
{
    "automobile": {
        "vin": "THISISMYPINKCAR99",
        "sold": false
    },
    "salesperson": {
        "first_name": "Ian",
        "last_name": "Pirro",
        "employee_id": "ipirro",
        "id": 4
    },
    "customer": {
        "first_name": "Mishka",
        "last_name": "Pirro",
        "address": "1234 30th Street, San Diego, CA 92102",
        "phone_number": "978-123-4567",
        "id": 3
    },
    "price": 13.99,
    "id": 9
}
```

NOTE: when a Sale is created on the front-end app, a PUT request is sent to the Automobile API in the inventory microservice to update that Automobile with `{"sold": True}`. AutomobileVOs are then updated automatically via the poller.

Deleting a Sale:
Send a DELETE request to http://localhost:8090/api/sales/:id, where :id is replaced with the id value of the Sale to be deleted. No JSON body required. On first request, a message will be returned confirming the deletion.
