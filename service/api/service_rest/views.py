from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "import_href"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vip",
        "vin",
        "customer",
        "technician"
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(employee_id=id).delete()
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=400,
            )
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content['technician']
            technician = Technician.objects.get(employee_id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=400,
            )
        sold_vins = AutomobileVO.objects.all()
        vins = []
        for vin in sold_vins:
            vins.append(vin.vin)
        if content["vin"] in vins:
            content["vip"] = True
        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )





@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=400
            )
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Canceled"
            appointment.save()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=400
            )
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["PUT"])
def api_finished_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "Finished"
            appointment.save()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=400
            )
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
