from django.urls import path
from .views import api_list_technicians, api_delete_technician, api_list_appointments, api_delete_appointment
from .views import api_cancel_appointment, api_finished_appointment

urlpatterns = [
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:id>/", api_delete_technician, name="delete_technicians"),
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", api_delete_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel/", api_cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish/", api_finished_appointment, name="finish_appointment"),
]
