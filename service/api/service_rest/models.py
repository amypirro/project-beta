from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)
    import_href =models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("vin",)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ("first_name",)


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=False)
    vip = models.BooleanField(default=False)
    reason = models.TextField(max_length=400)
    status = models.CharField(max_length=200, default="Created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey (
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("date_time",)
